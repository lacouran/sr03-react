/* eslint-disable array-callback-return */
const axios = require('axios');

const url = "https://webapplis.utc.fr/Edt_ent_rest/myedt/result/?login=";

export async function loadEDTByLogin(login) {
  const result = await axios.get(`${url + login}`)
    .then(({ data }) => data);
  return result;
}

export function loadUserUVs(edt) {
  let res = [];
  for (let i = 0; i < edt.length; i++) {
    if (!res.includes(edt[i].uv)) res.push(edt[i].uv);
  }
  return res;
}

const dayMapper = {
  'LUNDI' : 1,
  'MARDI' : 2,
  'MERCREDI' : 3,
  'JEUDI' : 4,
  'VENDREDI' : 5,
  'SAMEDI' : 6,
  'DIMANCHE' : 7,
}

function parseDate(begin, end, day) {
  let sep1 = begin.split(':');
  let sep2 = end.split(':');
  let today = new Date();
  let d1 = new Date(today.getFullYear(), today.getMonth(), dayMapper[day], parseInt(sep1[0]), parseInt(sep1[1]));
  let d2 = new Date(today.getFullYear(), today.getMonth(), dayMapper[day], parseInt(sep2[0]), parseInt(sep2[1]));
  return {
    begin: d1,
    end: d2,
  }
}

function isTp(type){
  if (type === 'TP') return 'FREQ=WEEKLY;INTERVAL=2';
  else return 'FREQ=WEEKLY'
}

function UTCtoSchedItem({ uv, begin, end, type , day , room , group } = {}, id, color) {
  let dates = parseDate(begin, end, day);
  return {
    Id: id,
    Subject: uv,
    StartTime: dates.begin,
    EndTime: dates.end,
    RecurrenceRule: isTp(type),
    Location: room + ' - ' + type,
    Description: group,
    Color: color,
  }
}

export function Mapper(data, student){
  let res = []
  data.forEach((utcObj) => {
    res.push(UTCtoSchedItem(utcObj, student.login, student.color));
  })
  return res;
}


