import React from 'react';
import Button from 'react-bootstrap/Button';

function Information({ uvs, onDelete }) {

  function handleDelete(input) {
    onDelete(input);
  }

  return (
    <div style={{display:'flex', flexDirection:'row'}}>
      {uvs.map((item) => (
        <div className="Logins">
          <label style={{ marginRight: '5px', fontWeight: 'bold', }}>{item.login} </label>
          <p style={{ marginRight: '15px', }}>{item.uvs.join(', ')}</p>
          <Button onClick={() => { handleDelete(item.login) }} variant="danger">Delete</Button>
        </div>
      ))}
    </div>
  )
}

export default Information;