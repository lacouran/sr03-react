import React, { useState } from 'react';
import Button from 'react-bootstrap/Button';
import Navbar from 'react-bootstrap/Navbar';
import Form from 'react-bootstrap/Form';
import FormControl from 'react-bootstrap/FormControl';

function SearchBar(props) {
  const [enteredLogin, setEnteredLogin] = useState('');


  return (
    <div className="SearchBar">
      <h1>{props.title}</h1>
      <Navbar bg="light" expand="lg">
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">

          <Form inline>
            <FormControl onChange={(event) => setEnteredLogin(event.target.value)} type="text" placeholder="Login" className="mr-sm-2" />
            <div className="Delete">
              <Button onClick={() => { props.onSearch(enteredLogin) }} variant="primary">Search</Button>
            </div>
            <Button onClick={() => { props.onDelete(enteredLogin) }} variant="danger">Delete</Button>
          </Form>
        </Navbar.Collapse>
      </Navbar>
      <div>
        <p style={{ fontWeight: 'bold' }}>Information</p>
      </div>
    </div>
  )
}

export default SearchBar;