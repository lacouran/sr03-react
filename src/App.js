/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from 'react';
import './App.css';
import randomColor from 'randomcolor';
import Information from './components/Information';
import SearchBar from './components/SearchBar';
import { Inject, ScheduleComponent, Day, Week, ViewsDirective, ViewDirective } from '@syncfusion/ej2-react-schedule';
import { loadEDTByLogin, loadUserUVs, Mapper } from './api/edtutc';



function App() {
  const [EDT, setEDT] = useState([]);
  const [uvs, setUvs] = useState([]);
  //const [searchLogins, setSearchLogins] = useState([]);
  const [student, setStudent] = useState({ login: 'lacouran', color: randomColor() });

  useEffect(() => {
    let canceled = false;
    loadEDTByLogin(student.login)
      .then((edt) => {
        if (!canceled || edt.length > 0) {
          let list_uvs = loadUserUVs(edt);
          let val = {
            login: student.login,
            uvs: list_uvs,
          }
          setUvs([...uvs, val]);
          let mapped_objects = Mapper(edt, student);
          setEDT(EDT.concat(mapped_objects));
        }
      })
      .catch(err => console.log(err));
    return () => { canceled = true; } //prevent quick api fetch spamming;
  }, [student])

  function handleSearch(input) {
    if (input === '' || contains(EDT, input) === true) return; //si l'utilisateur rentre une chaîne vide on sort de la fonction

    let student = {
      login: input,
      color: randomColor(),
    }
    setStudent(student);
  }

  function contains(arr, elementToCheck) { //Check if element is already searched;
    let found = false;
    for (let i = 0; i < EDT.length; i++) {
      if (EDT[i].Id === elementToCheck) {found = true; break};
    }
    return found;
  }

  function handleDelete(input) {
    if (input === '') return; //si l'utilisateur rentre une chaîne vide on sort de la fonction
    deleteEDT(input);
    deleteUVs(input);
  }


  function deleteEDT(login) {
    let newEDT = EDT.filter((element) => {
      return element.Id !== login;
    })
    setEDT(newEDT);
  }

  function deleteUVs(login) {
    let newUvs = uvs.filter((element) => {
      return element.login !== login;
    })
    setUvs(newUvs);
  }

  function onRenderCell(event) {
    let cell = event.data;
    event.element.style.backgroundColor = cell.Color;
  }

  return (
    <div className="App">
      <SearchBar onSearch={handleSearch} onDelete={handleDelete} title='Emploi du temps' />

      <Information onDelete={handleDelete} uvs={uvs} />


      <ScheduleComponent
        eventSettings={{ dataSource: EDT }}
        timeScale={{ interval: 60 }}
        showWeekNumber={true}
        startHour='7:00'
        endHour='21:00'
        height='600px'
        width='900px'
        eventRendered={onRenderCell}

      >
        <ViewsDirective>
          <ViewDirective option='Day' />
          <ViewDirective option='Week' />
        </ViewsDirective>
        <Inject services={[Day, Week]} />
      </ScheduleComponent>

    </div>
  );
}

export default App;
